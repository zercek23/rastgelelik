/**  
 *  
 * @author Burak Bayram - burakbyrm99@gmail.com, Alperen Örsdemir - alpne@hotmail.com
 * @since 14.03.2019
 * <p>  *  Bu program, yazılan RastgeleKarakter kütüphanesini test etmek amacıyla yazılmıştır.  * </p>  
 */ 
 
package Test;
import RastgeleKarakter.RastgeleKarakter;

public class Test {    
    public static void main(String[] args) {
        RastgeleKarakter a = new RastgeleKarakter();
        
        System.out.println("Rastgele Karakter: ");
        for (int i = 0; i < 100; i++) {
            System.out.print(a.RastgeleKarakterGetir() + " ");
        }
        System.out.println();
        System.out.println();
        
        System.out.println("Rastgele 3 Karakter: ");
        for (int i = 0; i < 100; i++) {
            System.out.print(a.RastgeleKarakterGetir(3));  
            System.out.print(" ");
        }
        System.out.println();
        System.out.println();
        
        System.out.println("Verilen iki karakter (a,k): ");
        for (int i = 0; i < 100; i++) {
            System.out.print(a.AraligaUygunKarakterGetir('a', 'k'));
            System.out.print(" ");
        }
        System.out.println();
        System.out.println();
        
        System.out.println("Verilen iki karakter (a,k): ");
        for (int i = 0; i < 100; i++) {
            System.out.print(a.AraligaUygunKarakterGetir('a', 'k', 3));
            System.out.print(" ");
        }
        System.out.println();        
        System.out.println();
        
        System.out.println("Belirtilen Karakterler (g,y,u,c,n,e): ");
        for (int i = 0; i < 100; i++) {
            System.out.print(a.BelirtilenKarakteriGetir('g','y','u','c','n','e'));
            System.out.print(" ");
        }
        System.out.println();
        System.out.println();
        
        System.out.println("Belirtilen Karakterler (g,y,u,c,n,e): ");
        for (int i = 0; i < 100; i++) {
            System.out.print(a.BelirtilenKarakteriGetir(3,'g','y','u','c','n','e'));
            System.out.print(" ");
        }
        System.out.println();
        System.out.println();
        
        System.out.println("Rastgele Cümle: "); // Cümle 10 kelimeyi geçemez, ayrıca her kelimenin harf sayısı da 10'u geçemez.
        System.out.println(a.CumleGetir());
        
    }
    
}
