/**  
 *  
 * @author Burak Bayram - burakbyrm99@gmail.com, Alperen Örsdemir - alpne@hotmail.com
 * @since 14.03.2019
 * <p>  *  Bu kütüphane, rastgele karakterler üretmesi için bir programda kullanmak üzere yazılmıştır.  * </p>  
 */ 

package RastgeleKarakter;

public class RastgeleKarakter { 
    
    public RastgeleKarakter() {
       
    }
    /**
     * Bu fonksiyon rastgele karakter döndürür.
     * @return 
     */
    public char RastgeleKarakterGetir(){
        char karakter = 0;
        long rastgeleSayi;
        
        do{ 
            //Verilen aralığa göre rastgele bir sayıyı değişkene atar.
            rastgeleSayi = System.nanoTime() % 123;
        }while(!((rastgeleSayi <= 90 && rastgeleSayi >=65) || (rastgeleSayi<=122 && rastgeleSayi >= 97)));
        
        karakter = (char)rastgeleSayi;
        
        return karakter;
    }
    /**
     * Bu fonksiyon verilen parametreye göre adedince rastgele karakter üretir.
     * @param adet
     * @return 
     */
    public char [] RastgeleKarakterGetir(int adet){
        char [] karakterler = new char[adet];
        
        for (int i = 0; i < adet; i++) {
            karakterler[i] = RastgeleKarakterGetir();
        }
        
        return karakterler;
    }
    /**
     * Parametrelerdeki harf aralığına göre rastgele karakter getirir.
     * @param a
     * @param b
     * @return 
     */
    public char AraligaUygunKarakterGetir(char a, char b){
        char donecekKarakter = 0;
        int ilk = (int)a;
        int son = (int)b;
        
        if (!((ilk == son+1) || (ilk+1 == son) || (ilk == son))) {
            do{
                donecekKarakter = RastgeleKarakterGetir();
            }while(!((donecekKarakter < b && donecekKarakter >a) || (donecekKarakter > b && donecekKarakter < a)));
        }
        
        return donecekKarakter;
    }
    /**
     * Parametrelerdeki harf aralığına göre adedince rastgele karakter getirir.
     * @param a
     * @param b
     * @param adet
     * @return 
     */
    public char [] AraligaUygunKarakterGetir(char a, char b, int adet){
        char [] donecekKarakterler = new char[adet];
        int ilk = (int)a;
        int son = (int)b;
        
        if (Math.abs(ilk-son) > adet) {
            for (int i = 0; i < adet; i++) {
                donecekKarakterler[i] = AraligaUygunKarakterGetir(a,b);
            }
        }
        
        return donecekKarakterler;
    }
    /**
     * Rastgele harfli, rastgele sayıda kelimeli cümle getirir.
     * @return 
     */
    public String CumleGetir(){        
        String cumle = "";
        long bosluk;
        long adet;  
                
        do{
            bosluk = System.nanoTime() % 123;
        }while(!(bosluk <= 10 && bosluk > 0));
        
        char [] karakterler = new char[100]; 
                                
        for (int i = 0; i < bosluk; i++) {
            do{
                adet = System.nanoTime() % 123;
            }while(!(adet <= 10 && adet > 0));
            for (int j = 0; j < adet; j++) {                                   
                karakterler[j] = RastgeleKarakterGetir();
                cumle = cumle + karakterler[j];                
            }
            cumle = cumle + " ";
        }
        
        return cumle;
    }
    /**
     * Parametrelere yazılan karakterlerden rastgele bir tanesini getirir.
     * @param a
     * @return 
     */
    public char BelirtilenKarakteriGetir(char... a){
        char karakter;
        long sayi;       
                
        do{
            sayi = (System.nanoTime() / 100) % 10;
        }while(!(sayi < a.length && sayi >= 0));
        
        karakter = a[(int)sayi];        
        
        return karakter;
    }
    /**
     * Parametrelere yazılan karakterlerden rastgele istenilen adedince karakter getirir.
     * @param adet
     * @param a
     * @return 
     */
    public char [] BelirtilenKarakteriGetir(int adet, char... a){
        char [] karakterler = new char[adet];
        long sayi;
                
        for (int j = 0; j < adet; j++) {
                do{
                    sayi = (System.nanoTime() / 100) % 10;
                }while(!(sayi < a.length && sayi >= 0));
            karakterler[j] = a[(int)sayi];
        }
        
        return karakterler;
    }        
}
